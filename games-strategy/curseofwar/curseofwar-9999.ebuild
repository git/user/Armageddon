# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit games git-2

DESCRIPTION="Real Time Strategy Game for Linux"
HOMEPAGE="https://github.com/a-nikolaev/curseofwar"
EGIT_REPO_URI="https://github.com/a-nikolaev/${PN}.git"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="sdl"

RDEPEND="
	app-shells/bash
	sys-libs/ncurses
	sdl? ( media-libs/libsdl )"
DEPEND="${RDEPEND}
	sys-devel/gcc"

src_configure() {
	sed -i -e 's/CFLAGS\ \ =/CFLAGS\ \ \+=/' Makefile || \
		die "couldn't patch Makefile CFLAGS"
	sed -i -e 's/LDFLAGS\ =/LDFLAGS\ \+=/' Makefile || \
		die "couldn't patch Makefile LDFLAGS"
}

src_compile() {
	local myconf=""

	if use sdl; then
		myconf+="SDL=yes"
	fi

    emake ${myconf}
}
