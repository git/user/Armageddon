# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

DESCRIPTION=""
HOMEPAGE=""
SRC_URI=""

LICENSE="GPL-2"
SLOT="1"
KEYWORDS="~amd64 ~x86"
IUSE="X doc test"

DEPEND=""
RDEPEND="${DEPEND}"
S="${WORKDIR}"

src_install() {
	dodir /var/lib/"${P}"/
	touch "${ED}"/var/lib/"${P}"/installed
	if use X;
	then
		touch "${ED}"/var/lib/"${P}"/X
	fi
	if use doc;
	then
		touch "${ED}"/var/lib/"${P}"/doc
	fi
	if use test;
	then
		touch "${ED}"/var/lib/"${P}"/test
	fi
}
