# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit games cmake-utils git-2

DESCRIPTION="A free 2D Zelda fangame"
HOMEPAGE="http://www.solarus-games.org/"
EGIT_REPO_URI="git://github.com/christopho/${PN}.git
	https://github.com/christopho/${PN}.git"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""
IUSE=""

RDEPEND="=games-engines/solarus-9999"
DEPEND="${RDEPEND}"

src_install() {
	cmake-utils_src_install
	newicon -s 48 "build/icons/${PN}_icon_48.png" "${PN}.png"
	make_desktop_entry "${PN}" "Zelda: Mystery of Solarus DX" "${PN}.png"
}
