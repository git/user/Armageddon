# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit games cmake-utils

DESCRIPTION="A free 2D Zelda fangame"
HOMEPAGE="http://www.solarus-games.org/"
SRC_URI="http://www.zelda-solarus.com/downloads/${PN}/${P}.tar.gz
	https://raw.github.com/christopho/${PN}/master/build/icons/${PN}_icon_48.png"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

RDEPEND="=games-engines/solarus-1.0.4"
DEPEND="${RDEPEND}"

src_install() {
	cmake-utils_src_install
	newicon -s 48 "${DISTDIR}/${PN}_icon_48.png" "${PN}.png"
	make_desktop_entry "${PN}" "Zelda: Mystery of Solarus DX" "${PN}.png"
}
