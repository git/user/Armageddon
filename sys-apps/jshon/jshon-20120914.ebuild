# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

DESCRIPTION="A json parser for the shell"
HOMEPAGE="http://kmkeen.com/jshon/"
SRC_URI="http://kmkeen.com/jshon/${PN}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

RDEPEND="dev-libs/jansson"
DEPEND="${RDEPEND}
		sys-devel/gcc"

src_install() {
	dobin ${PN}
	doman ${PN}.1
}
