# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit eutils autotools gnome2-utils

DESCRIPTION=" GdMap is a tool which allows to visualize disk space"
HOMEPAGE="http://gdmap.sourceforge.net/"
SRC_URI="mirror://sourceforge/project/${PN}/${PN}/${PV}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="dev-libs/glib:2
x11-libs/gtk+:2
dev-libs/libxml2:2"
DEPEND="${RDEPEND}
dev-util/intltool
sys-devel/gettext"

src_prepare() {
	epatch "${FILESDIR}/gtk-fix.patch"
	eautoreconf
}

src_install() {
	dobin src/${PN}
	doicon -s 256 data/${PN}_icon.png
	make_desktop_entry gdmap GdMap gdmap_icon "Filesystem;Utility;GTK;"
	doman data/${PN}.1
	domo po/*.gmo
}

pkg_preinst() {
	gnome2_icon_savelist
}

pkg_postinst() {
	gnome2_icon_cache_update
}

pkg_postrm() {
	gnome2_icon_cache_update
}
