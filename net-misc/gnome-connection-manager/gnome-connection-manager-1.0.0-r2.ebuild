# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4
PYTHON_DEPEND="2"

inherit python eutils

DESCRIPTION="Gnome Connection Manager is a tabbed ssh connection manager for gtk"
HOMEPAGE="http://kuthulu.com/gcm/"
SRC_URI="http://kuthulu.com/gcm/gnome-connection-manager_${PV}_all.tgz -> ${P}.tgz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="
	dev-python/pygtk
	dev-tcltk/expect
	dev-util/glade[python]
	gnome-base/libglade:2.0
	net-misc/openssh
	x11-libs/vte[python]"
RDEPEND="${DEPEND}"

S="${WORKDIR}"/gcm-"${PV}"

pkg_setup() {
	python_set_active_version 2
	python_pkg_setup
}

src_prepare() {
	sed -i "s|BASE_PATH = os.path.*|BASE_PATH = '/usr/share/"${PN}"'|" gnome_connection_manager.py \
		|| die "Can not apply patch to BASE_PATH"
	sed -i "s|glade_dir = \"\"|glade_dir = BASE_PATH|" gnome_connection_manager.py \
		|| die "Can not apply patch to glade_dir"
	sed -i "s|env python\s*$|env python2|" gnome_connection_manager.py \
		|| die "Can not apply patch to python"
}

src_install() {
	insinto /usr/share/"${PN}"
	doins -r donate.gif gnome-connection-manager.glade \
		gnome_connection_manager.py lang pyAES.py \
		SimpleGladeApp.py SimpleGladeApp.pyc ssh.expect icon.png
	fperms 755 /usr/share/"${PN}"/gnome_connection_manager.py
	dosym /usr/share/"${PN}"/gnome_connection_manager.py /usr/bin/"${PN}"
	make_desktop_entry "${PN}"  'Gnome Connection Manager' \
		"/usr/share/${PN}/icon.png" 'GNOME;GTK;Network;'
	newdoc README.txt README
}
