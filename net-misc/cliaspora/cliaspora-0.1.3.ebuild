# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

DESCRIPTION="Command-line client for DIASPORA*"
HOMEPAGE="https://freeshell.de//~mk/projects/cliaspora.html"
SRC_URI="https://freeshell.de//~mk/download/cliaspora-0.1.3.tgz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="dev-libs/openssl"
RDEPEND="${DEPEND}"

src_install() {
	dobin "${PN}"
	doman "${PN}".1
}
