# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit games cmake-utils

DESCRIPTION="An open-source Zelda-like 2D game engine"
HOMEPAGE="http://www.solarus-games.org/"
SRC_URI="http://www.zelda-solarus.com/downloads/${PN}/${P}-src.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

RDEPEND="
	media-libs/libmodplug
	media-libs/libvorbis
	dev-lang/lua
	media-libs/openal
	dev-games/physfs
	media-libs/sdl-image
	media-libs/sdl-ttf"
DEPEND="${RDEPEND}"
