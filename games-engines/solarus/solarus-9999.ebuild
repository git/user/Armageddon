# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit games cmake-utils git-2

DESCRIPTION="An open-source Zelda-like 2D game engine"
HOMEPAGE="http://www.solarus-games.org/"
EGIT_REPO_URI="git://github.com/christopho/${PN}.git
	https://github.com/christopho/${PN}.git"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""
IUSE=""

RDEPEND="
	media-libs/libmodplug
	media-libs/libvorbis
	dev-lang/lua
	media-libs/openal
	dev-games/physfs
	media-libs/sdl2-image
	media-libs/sdl2-ttf"
DEPEND="${RDEPEND}"
